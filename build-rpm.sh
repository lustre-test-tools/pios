PKG=pios
VERSION=1.2
PKG_NAME=$PKG-$VERSION
TOPDIR=/tmp/rpmbuild
rm -rf $TOPDIR
rm -f *.rpm
mkdir -p $TOPDIR/SOURCES

tar czf $TOPDIR/SOURCES/$PKG_NAME.tar.gz $PKG_NAME
cp *.patch $TOPDIR/SOURCES/
rpmbuild --define "_topdir $TOPDIR" -bs $PKG.spec

cp $TOPDIR/SRPMS/*.src.rpm .
rpmbuild --define "_topdir $TOPDIR" --define "_bindir /usr/local/bin/" --undefine=_debugsource_packages --rebuild *.src.rpm
cp $TOPDIR/RPMS/*/* .