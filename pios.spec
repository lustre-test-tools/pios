Summary: Parallel IO simulator.
name: pios
Version: 1.2
Release: 1%{?dist}
License: GPL
Group: Utilities/Benchmarking
URL: http://downloads.whamcloud.com/public/tools/
Source: %{name}-%{version}.tar.gz
Patch1: fc18-autoconf-fixup.patch
BuildRoot: /tmp/%{name}-buildroot
BuildRequires: e2fsprogs-devel
%if %{?el6}0 > 0
BuildRequires: libblkid-devel
%endif

%description
 Pios performs runs. Each run starts threadcount threads and writes to
regioncount areas in one or more files. If -fpp flag is given, pios writes or
reads to regioncount files in path given. If the final argument is a file or
block device, pios will write to regioncount regions in one file. Pios issues
IO commands of size chunksize. The regions are spaced apart offset bytes (or
in the case of many files, the region starts at offset bytes). In each region
regionsize bytes are written or read.

Multiple runs can be specified with comma separated lists of values for
chunksize, offset, regioncount, threadcount, regionsize. Multiple runs can also
be specified by giving a starting (low) value, increase (in percent) and high
value for each of these arguments. If a low value is given, no value list or
value may be supplied.

Every run is given a timestamp, and the time stamp and offset is written with
every chunk, to allow verification. Before every run pios will run the prerun
shell command, after each run the postrun command. This is typically used to
clear and collect statistics for the run, or to start and stop statistics
gathering during the run. Both prerun and postrun are passed the timestamp.

For convenience pios understands a byte specificers using K,k for kilo bytes
(2<<10), M,m for megabytes (2<<20), G,g for gigabytes (2<<30) and T,t for
terabytes (2<<40).

The currently supported IO methods are directio, posixio, cowio.

%prep
%setup -q
%patch1 -p1

%build
autoreconf --install
./configure
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p ${RPM_BUILD_ROOT}/{%{_bindir},%{_mandir}/man8}
install -s -m 755 pios $RPM_BUILD_ROOT%{_bindir}
install -m 644 pios.8 $RPM_BUILD_ROOT%{_mandir}/man8/pios.8

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README INSTALL COPYING
%{_mandir}/man8/pios.8.gz
%{_bindir}/pios

%changelog
* Fri Nov 4 2011 Minh Diep <mdiep@whamcloud.com>
- first packaging
