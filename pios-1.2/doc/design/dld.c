#include <stdint.h>
#include <pthreads.h>


/* extract an unsigned int and K,M,G,T from the string,
  multiply with << 10,20,30,40
*/
u64 pios_interpret_KMGT(char *string, maybe length);

/* all offsets, sizes and counts can be passed to the application in
  multiple ways.
  1. a value (stored in val[0], val_count will be 1)
  2. a comma separated list of values (stored in val[], using val_count)
  3. a range and block sizes, low, high, factor (val_count must be 0)
*/
struct pios_range_repeat {
 u64 val[32];
 u32 val_count;
 u64 val_low;
 u64 val_high;
 u32 val_inc_perc;
};


/* all arguments collected together */
struct pios_args {
 struct pios_range_repeat T; /* threads */
 struct pios_range_repeat N; /* region count */
 struct pios_range_repeat O; /* offsets */
 struct pios_range_repeat C; /* chunksize */
 struct pios_range_repeat S; /* regionsize */
 .... all other processed arguments ...
} pios_args;

/* a region for IO */
struct pios_stream {
 u64 offset;
 u64 max_offset;
 fd_t fd;
 spinlock_t lock;
}

/* arguments for one run */
struct run_arg {
 u64 chunksize;
 u32 threadcount;
 u32 regioncount;
 u64 regionsize;
 u64 offset;
 u32 regionnoise;
 u32 chunknoise;
 u32 threaddelay;
 struct pios_stream[0];
};

void setup_run_arg(struct run_arg *arg, u64 T, u64 N, u64 C, u64 S,...)
{
 ASSERT(arg = malloc(sizeof(*arg + N * sizeof(struct pios_stream))));
 setup_streams(arg, N);
}

int thread_main()
{
 while(get_work_item(....)) {
   rc = do_io();
   if (rc)
     return rc;
 }
 return 0;
}

void start_threads(...)
{


}


void do_one_run(u64 T, u64 N, u64 C, u64 S,...)
{
 u32 timestamp = getimeofday();
 struct pios_stream *streams;
 struct run_arg arg;

 setup_run_arg(&arg);
 call_script(pre_run, timestamp);
 start_threads(..);
 wait_for_results();
 call_script(post_run, timestamp);
 cleanup_threads();
 cleanup_run_arg();
}


void do_all_runs(struct pios_args *args)
{
 u64 T, N, C, S, O;


 REPEAT(T, &args.T) {
   REPEAT(N, &args.N) {
     REPEAT(C, &args.C) {
       REPEAT(S, &args.S) {
         REPEAT(O, &args.O) {
           do_one_run(T,N,C,S,O, ...);
         }
       }
     }
   }
 }
 return 0;
}


int main(char **argv, int argc)
{
 interpret_args(argv, argc); /* fill in pios_args */
 do_all_runs(&pios_args);
 return 0;
}
