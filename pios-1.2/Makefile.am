#
#  Copyright (C) 2007 Cluster File Systems, Inc.
#
#   This file is part of Lustre, http://www.lustre.org.
#
#   Lustre is free software; you can redistribute it and/or
#   modify it under the terms of version 2 of the GNU General Public
#   License as published by the Free Software Foundation.
#
#   Lustre is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Lustre; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

PIOS_CFLAGS = -Wall -static -g -D_GNU_SOURCE
RPMBUILD=rpmbuild
PIOS_DIR=$(PWD)
#Tag of pios cvs source
VERSION=1.2

if LINUX

if ENABLE_DMU
        DMU_CFLAGS = -DENABLE_DMU
        PIOS_LIBS = -L $(ZFS_FUSE_SRC)/src/lib/libzpool -lzpool \
                    -L $(ZFS_FUSE_SRC)/src/lib/libnvpair -lnvpair \
                    -L $(ZFS_FUSE_SRC)/src/lib/libavl -lavl \
                    -L $(ZFS_FUSE_SRC)/src/lib/libzfscommon -lzfscommon \
                    -L $(ZFS_FUSE_SRC)/src/lib/libsolcompat -lsolcompat \
                    -L $(ZFS_FUSE_SRC)/src/lib/libumem -lumem \
                    -lpthread -lm -ldl -lrt -lz
else
        PIOS_LIBS = -lpthread
        DMU_CFLAGS = 
endif
rpm:
	@if [ -s pios-$(VERSION).tar.gz ] ; then \
	 echo " pios-$(VERSION).tar.gz found in current directory, removing \
	 it." ; \
         @rm -f pios-$(VERSION).tar.gz ; \
	fi ;
# Make sure that the directory you have pios source in is named as
# pios-$(VERSION) (e.g. pios-1.0.2). If directory is present in above
# mentioned way then tarball will be created and passed to
# "make rpm",
	if [ -s ../pios-$(VERSION) ] ; then \
		tar -czvf pios-$(VERSION).tar.gz ../pios-$(VERSION) ; \
	else \
         echo " Error: Current working directory is $(PIOS_DIR), it should be pios-$(VERSION)." ; \
	 exit 1 ; \
	fi;
	rm -rf dist/
	mkdir -p dist/BUILD \
		 dist/SPECS \
		 dist/RPMS \
                 dist/SOURCES \
                 dist/TMP \
                 dist/install \
                 dist/SRPMS
	cp $(PWD)/pios.spec.in $(PWD)/dist/SPECS/pios.spec
	ln -s $(PWD)/pios-$(VERSION).tar.gz  $(PWD)/dist/SOURCES/pios-$(VERSION).tar.gz

	$(RPMBUILD) -bb \
            --define "_topdir $(PWD)/dist" \
            --define "buildroot $(PWD)/dist/install" \
            $(RPMOPT) \
            $(PWD)/dist/SPECS/pios.spec

	cp dist/*RPMS/*/*.rpm .

else
    PIOS_LIBS = -lzpool -lnvpair -lpthread 
    DMU_CFLAGS = -DENABLE_DMU
endif


bin_PROGRAMS  = pios
pios_SOURCES  = pios.c udmu/udmu.c
pios_CPPFLAGS = $(PIOS_CFLAGS) $(DMU_CFLAGS)
pios_LDADD    = $(PIOS_LIBS)
