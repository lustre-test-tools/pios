/* -*- mode: c; c-basic-offset: 8; indent-tabs-mode: nil; -*-
 * vim:expandtab:shiftwidth=8:tabstop=8:
 *
 *  Copyright (C) 2004 Cluster File Systems, Inc.
 *  Authors: Peter Braam <braam@clusterfs.com>
 *           Atul Vidwansa <atul@clusterfs.com>
 *           Milind Dumbare <milind@clusterfs.com>
 *
 *   This file is part of Lustre, http://www.lustre.org.
 *
 *   Lustre is free software; you can redistribute it and/or
 *   modify it under the terms of version 2 of the GNU General Public
 *   License as published by the Free Software Foundation.
 *
 *   Lustre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Lustre; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <assert.h>
#include <errno.h>

#include <string.h>
#include <getopt.h>
#include <limits.h>
#include <time.h>

#include <regex.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/vfs.h>
#include <sys/ioctl.h>
#include <sys/statvfs.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <sys/time.h>
#include <sys/resource.h>

#if defined(linux)
#include <linux/fs.h>
#include <mntent.h>
#include <blkid/blkid.h>
#include <sys/time.h>
#include <malloc.h>
#elif defined(__sun__) || defined(__sun)
#include <sys/mntent.h>
#include <sys/uio.h>
#endif

#include "udmu/udmu.h"

#define __u64 uint64_t
#define __u32 uint32_t

/* IO types */
#define COWIO 1
#define DIRECTIO 2
#define POSIXIO 4
#define DMUIO 8          /* ZFS DMU mode */

/* Regular expressions */
#define REGEX_NUMBERS "^[0-9]*[0-9]$"
#define REGEX_NUMBERS_COMMA "^([0-9]+,)*[0-9]+$"
#define REGEX_SIZE "^[0-9][0-9]*[kmgt]$"
#define REGEX_SIZE_COMMA "^([0-9][0-9]*[kmgt]+,)*[0-9][0-9]*[kmgt]$"

/* Flags for low, high, incr */
#define FLAG_LOW 2
#define FLAG_HIGH 4
#define FLAG_INCR 8

#define TRUE 1
#define FALSE 0

/* getopt */
extern int optind;

void print_usage()
{
        printf("\nUsage:\n");
        printf("pios\n");
        printf("[--chunksize|-c =values, (--chunksize_low|-a =value "
               "--chunksize_high|-b =value --chunksize_incr|-g =value)]\n");

        printf("[--offset|-o =values, (--offset_low|-m =value"
               " --offset_high|-q =value --offset_incr|-r =value)]\n");
 
        printf("[--regioncount|-n =values, (--regioncount_low|-i =value "
               "--regioncount_high|-j =value --regioncount_incr|-k =value)]\n");

        printf("[--threadcount|-t =values, (--threadcount_low|-l =value "
               "--threadcount_high|-h =value --threadcount_incr|-e =value)]\n");

        printf("[--regionsize|-s =values, (--regionsize_low|-A =value "
               "--regionsize_high|-B =value --regionsize_incr|-C =value)]\n");

#ifdef ENABLE_DMU
        printf("[--load|-L=(directio|posixio|dmuio)(,cowio)(,fpp|sff)]\n");
#else
        printf("[--load|-L=(directio|posixio)(,cowio)(,fpp|sff)]\n");
#endif
        printf("[--cleanup|-x --verify|-V --threaddelay|-T =ms "
               "--regionnoise|-I =shift --chunknoise|-N =bytes]\n");

        printf("[--prerun|-P =pre-command --postrun|-R =post-command]\n");
        printf("[-runno|-U =run number]\n");
        printf("[--path|-p =output-file-path|pool-name]\n");

}

void* pios_malloc(int size)
{
        void *ptr;
        ptr = malloc(size);
        if (ptr == NULL) {
                fprintf(stderr, "Error: Couldn't malloc\n");
                assert(0);
        }
        return ptr;
}

/* extracts an unsigned int (64) and K,M,G,T from the strin */
/* converts string to kilo-tera units */
__u64 pios_interpret_KMGT(const char *number_string)
{
        __u32 length;
        __u64 number;
        length = strlen(number_string);
        number = atoll(number_string);
        switch(number_string[length - 1]) {
                case 'k':
                case 'K':
                        number = number << 10;
                        break;
                case 'm':
                case 'M':
                        number = number << 20;
                        break;
                case 'g':
                case 'G':
                        number = number << 30;
                        break;
                case 't':
                case 'T':
                        number = number << 40;
                        break;
        }
        return number;
}

/* All offsets, sizes and counts can be passed to the application in
 * multiple ways.
 * 1. a value (stored in val[0], val_count will be 1)
 * 2. a comma separated list of values (stored in val[], using val_count)
 * 3. a range and block sizes, low, high, factor (val_count must be 0) */
struct pios_range_repeat {
        __u64 val[32];  /* array of values if comma separated or low, high, inc 
                        * given */
        __u32 val_count; /* no of values if comma separated or low, high, inc
                          * given */ 
        __u64 val_low;  /* low value among low, high, inc given */ 
        __u64 val_high;  /* high value among low, high, inc given */
        __u32 val_inc_perc;  /* inc among low, high, inc given */ 
        __u32 next_val; /* this is used for multiple runs in get_next()*/
};

/* all arguments collected together */
struct pios_args {
        struct pios_range_repeat T; /* threads */
        struct pios_range_repeat N; /* region count */
        struct pios_range_repeat O; /* offsets */
        struct pios_range_repeat C; /* chunksize */
        struct pios_range_repeat S; /* regionsize */
        struct pios_range_repeat V; /* if verify flag has more than one stamps 
                                     * for iterative runs */
        const char *path; /* test directory path */
        __u32 fpp; /* if set regioncount should be considerd as no of files */
        __u32 io_type; /* COWIO, POSIXIO, DIRECTIO */

        /* three noises that can be added */
        __u32 regionnoise;
        __u64 chunknoise;
        __u32 thread_delay;

        /* before and after run commands */
        const char *prerun;
        const char *postrun;

        __u32 cleanup; /* cleanup the files created during test */
} pios_args;

/* a region for IO */
struct pios_stream {
        __u64 offset;
        __u64 max_offset;
        int fd;
        udmu_obj_t obj;
};

/* arguments for one run */
struct run_arg {
        int runno;
        __u64 chunksize;
        __u32 threadcount;
        __u32 regioncount;
        __u64 regionsize;
        __u64 offset;
        __u32 regionnoise;
        __u32 chunknoise;
        __u32 thread_delay;
        __u64 verify;
        time_t timestamp;
        __u32 io_type;
        const char *path;
        struct pios_stream stream[0];
};

struct work_item_control {
        __u32 next_region;
} work_item_control;


pthread_t *thread;
pthread_barrier_t *barrier;
/* lock used for get_work_items */
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

/* lock used while recording time spent on writing each region */
pthread_mutex_t lock_time_region = PTHREAD_MUTEX_INITIALIZER;

/* lock used while recording total amount of data written in case of chunknoise
 * used */
pthread_mutex_t lock_data_written = PTHREAD_MUTEX_INITIALIZER;

/* lock used while reading timestamp from first location of file or from 
 * optional argument to --verify */
pthread_mutex_t lock_timestamp = PTHREAD_MUTEX_INITIALIZER;

/*data passed to each thread */
struct thread_data{
        struct run_arg *arg;
        int thread_no;
};

double **time_per_region;
double real_time;
int is_block_device = 0;
time_t main_timestamp = 0;
time_t timestamp_of_run = 0;

/* varible used to cross check the chunks read written during test run & 
 * --verify */
__u64 chunks_read_write = 0;

/* In case of chunknoise actual data written should
 * be calculated to find throughput */
__u64 data_written = 0; 

/* finds higher lower and average of time required to read/write each region */
void find_higher_lower(struct run_arg *arg, double *highest,
                       double *lowest, double *average)
{
        int i;
	*lowest = *time_per_region[0];
	*highest = *time_per_region[0];
	*average = 0;

	for (i=0; i<arg->regioncount; i++) {
		*average = *average + *time_per_region[i];
		if(*time_per_region[i] <= *lowest)
			*lowest = *time_per_region[i];
		else if(*time_per_region[i] >= *highest)
			*highest = *time_per_region[i];
	}
	*average = *average/arg->regioncount;

        return;
}

/* converts number to string postfixed with MB */
void in_MBs(char* string, __u64 number)
{
        number = number / 1048576;
        sprintf(string, "%llu MB", number);
        return;
} 

void print_stats(struct run_arg *arg, int timestamp)
{
	double total_time = 0;
	double highest = 0;
	double lowest = 0;
	double average = 0;

        char string[32];
        int i;
        __u64 total_data;

        printf("%d\t", arg->runno);
        printf("%d\t", timestamp);
        printf("%d\t", arg->threadcount);
        printf("%d\t", arg->regioncount);
        in_MBs(string, arg->regionsize);
        printf("%s\t", string);
        in_MBs(string, arg->chunksize);
        printf("%s\t", string);
        
        if(!arg->chunknoise) 
                total_data = arg->regionsize * arg->regioncount;
        else
                total_data = data_written; 

        system("> time.out");
	for (i = 0; i < arg->regioncount; i++) {
		total_time = total_time + *time_per_region[i];
                sprintf(string, "echo %2.3f >> time.out", *time_per_region[i]);
                system(string);
	}
	find_higher_lower(arg, &highest, &lowest, &average);
	in_MBs(string, (total_data / real_time));
        printf("\t%s / sec.\t", string);
        printf("%2.3f second(s).\t", lowest);
        printf("%2.3f second(s).\t", highest);
        printf("%2.3f second(s).\n",real_time);
}

/* matches the string with RE pattern, returns 1 on success and 0 on error */
int regex_match(const char *string, char *pattern)
{
        int status;
        regex_t re;

        if (regcomp(&re, pattern,  REG_EXTENDED | REG_NOSUB| REG_ICASE) != 0) {
                fprintf(stderr, "Error: Couldn't do regcomp\n");
                return(0);      /* report error */
        }

        status = regexec(&re, string, (size_t) 0, NULL, 0);
        regfree(&re);

        if (status != 0) {
                /* error */
                return(0);
        }
        return(1);
}

/* ioflag_given is set on each flag on command line, if multiple flags are
 * given ioflag_given value tends to be greater than one, in that case 
 * error msg must be displayed */ 
int set_ioflag(__u32* ioflag_given, int io_type)
{
        *ioflag_given |= io_type;
        if (*ioflag_given & POSIXIO)
                if(*ioflag_given & DIRECTIO) {
                        fprintf(stderr, "Error: Only one load from posixio, "
                                "directio can be given at a time\n");
                        assert(FALSE);
                }
        return 1;
}

/* fills the pios_range_repeat structure of comma separated values */
int split_string(const char* optarg, char* pattern, 
                 struct pios_range_repeat* range)
{
        const char comma[1] = ",";
        char* token[32];
        char *cp = pios_malloc(sizeof(char) * strlen(optarg));
        int i = 0, j;
        int values_separated_by_comma;

        if (!regex_match(optarg, pattern))  {
                return(-1);
        }

        strcpy(cp, optarg);                /* Make writable copy.  */

        do {
        /* strtok itself allocates memory for pointer to token so no need
         * of malloc here */
        token[i] = strtok(cp, comma);
        cp = NULL;
        } while (token[i++] != NULL);

        range->val_count = i - 1;
        values_separated_by_comma = i;
        assert(values_separated_by_comma < 32);

        for (j = 0; j < range->val_count; j++) {
                range->val[j] = pios_interpret_KMGT(token[j]);
        }

        free(cp);
        return 1;
}

/* validates the value with regular expression and sets low, high, incr
 * according to value at which flag will be set. Also sets the flag after
 * that */
void set_low_high_incr(char* pattern, struct pios_range_repeat* range,
                      char* optarg, int flag_to_set_at, __u32 *flag_thread,
                      char* case_from)
{

        if (!regex_match(optarg, pattern))
                fprintf(stderr, "Error: Wrong pattern in %s value\n", case_from);
        switch(flag_to_set_at) {
                case 2: 
                        range->val_low = pios_interpret_KMGT(optarg);
                        break;
                case 4: 
                        range->val_high = pios_interpret_KMGT(optarg);
                        break;
                case 8:
                        range->val_inc_perc = pios_interpret_KMGT(optarg);
                        break;
                }
        *flag_thread |= flag_to_set_at;
        return;
}

/* checks the low, high, increment values against the single value for 
 * mutual exclusion, for e.g threadcount is mutually exclusive to 
 * threadcount_low, ..._high, ..._incr*/
void check_mutual_exclusive_command_lines(__u32 flag, char* argument)
{
        if ((flag & 1) == TRUE)
                if ((flag & 2) == 2 || (flag & 4) == 4 ||(flag & 8) == 8) {
                        fprintf(stderr, "Error: --%s can not be given with "
                                "--%s_low, --%s_high or --%s_incr.\n",
                                argument, argument, argument, argument);
                        print_usage();
                        assert(FALSE);
                }
        /* neither low,high,incr nor one value */
        if ((flag & 14) != 14 && (flag & 1) != 1)
                if (flag >= 2) {/* We don't need followin error msg if there is no
                               * command line at all */
                        fprintf(stderr, "Error: One or more values missing from"
                                " --%s_low, --%s_high, --%s_incr.\n", argument,
                                argument, argument);
                        print_usage();
                        assert(FALSE);
                }
        return;
}

int parse_load_params(const char *optarg, struct pios_args *pios_args)
{
        char *search = strdup(optarg);
        char *coma = ",";
        char *param;

        int rc = 0;

        while((param = strtok(search, coma)) != NULL){
                search = NULL;
                if (strcmp("fpp", param) == 0) {
                        /* File Per Process/Thread */
                        pios_args->fpp = 1;
                } else if (strcmp("sff", param) == 0) {
                        /* Shared Shared File */
                        pios_args->fpp = 0;
                } else if (strcmp("cowio", param) == 0) {
                        set_ioflag(&pios_args->io_type, COWIO);
                } else if (strcmp("posixio", param) == 0) {
                        set_ioflag(&pios_args->io_type, POSIXIO);
                } else if (strcmp("directio", param) == 0) {
                        set_ioflag(&pios_args->io_type, DIRECTIO);
#ifdef ENABLE_DMU
                } else if (strcmp("dmuio", param) == 0) {
                        set_ioflag(&pios_args->io_type, DMUIO);
#endif
                } else {
                        printf("Invalid load: %s\n", param);
                        rc = -EINVAL;
                }
        }

        if (search) {
                free(search);
        }

        return (rc);
}

void interpret_args(int argc, char **argv, struct pios_args *pios_args)
{
        int c;
        __u32 flag_thread = 0;
        __u32 flag_regioncount = 0;
        __u32 flag_offset = 0;
        __u32 flag_regionsize = 0;
        __u32 flag_chunksize = 0;
        int longopt_index;
 
        struct option pios_long_opts[] = {
                {"threadcount",         required_argument, 0, 't'},
                {"threadcount_low",     required_argument, 0, 'l'},
                {"threadcount_high",    required_argument, 0, 'h'},
                {"threadcount_incr",    required_argument, 0, 'e'},
                {"regioncount",         required_argument, 0, 'n'},
                {"regioncount_low",     required_argument, 0, 'i'},
                {"regioncount_high",    required_argument, 0, 'j'},
                {"regioncount_incr",    required_argument, 0, 'k'},
                {"chunksize",           required_argument, 0, 'c'},
                {"chunksize_low",       required_argument, 0, 'a'},
                {"chunksize_high",      required_argument, 0, 'b'},
                {"chunksize_incr",      required_argument, 0, 'g'},
                {"regionsize",          required_argument, 0, 's'},
                {"regionsize_low",      required_argument, 0, 'A'},
                {"regionsize_high",     required_argument, 0, 'B'},
                {"regionsize_incr",     required_argument, 0, 'C'},
                {"offset",              required_argument, 0, 'o'},
                {"offset_low",          required_argument, 0, 'm'},
                {"offset_high",         required_argument, 0, 'q'},
                {"offset_incr",         required_argument, 0, 'r'},
                {"path",                required_argument, 0, 'p'},
                {"load",                required_argument, 0, 'L'},                
                {"cleanup",             no_argument,       0, 'x'},
                {"prerun",              required_argument, 0, 'P'},
                {"postrun",             required_argument, 0, 'R'},
                {"regionnoise",         required_argument, 0, 'I'},
                {"chunknoise",          required_argument, 0, 'N'},
                {"threaddelay",         required_argument, 0, 'T'},
                {"verify",              no_argument, 0, 'V'},
                {0, 0, 0, 0}
        };

                while ((c = getopt_long(argc, argv, "t:l:h:e:n:i:j:k:c:u:a:b:g:L:P:R:I:"
                                        "T:Vs:A:B:C:o:m:q:r:fwxdp:?",
                                        pios_long_opts, &longopt_index)) != -1)
                switch (c) {
                        /* threadcount */
                        case 't':
                                /* setting flag to tell that threadcount is 
                                 * given on command line */
                                flag_thread |= 1;

                                if (regex_match(optarg, REGEX_NUMBERS)) {
                                        pios_args->T.val[0] = 
                                        pios_interpret_KMGT(optarg);
                                        pios_args->T.val_count = 1;
                                } else if(split_string(optarg, 
                                                     REGEX_NUMBERS_COMMA,
                                                     &pios_args->T) == -1) {
                                        fprintf(stderr, "Error: Incorrect input"
                                                " pattern for threadcount\n");
                                        print_usage();
                                        assert(FALSE);
                                }
                                
                                break;
                        /* threadcount_low */
                        case 'l':
                                set_low_high_incr(REGEX_NUMBERS, &pios_args->T,
                                                  optarg, FLAG_LOW, &flag_thread, 
                                                  "threadcount_low");
                                break;
                        /* threadcount_high */
                        case 'h':
                                set_low_high_incr(REGEX_NUMBERS, &pios_args->T,
                                                  optarg, FLAG_HIGH, &flag_thread, 
                                                  "threadcount_high");
                                break;
                        /* threadcount_inc */
                        case 'e':
                                set_low_high_incr(REGEX_NUMBERS, &pios_args->T,
                                                  optarg, FLAG_INCR, &flag_thread, 
                                                  "threadcount_incr");
                                break;

                        /* regioncount */
                        case 'n':
                                flag_regioncount |= 1;
                                if  (regex_match(optarg, REGEX_NUMBERS)) {
                                        pios_args->N.val[0] = 
                                        pios_interpret_KMGT(optarg);

                                        pios_args->N.val_count = 1;
                                } else if(split_string(optarg, 
                                                     REGEX_NUMBERS_COMMA,
                                                     &pios_args->N) == -1) {
                                        fprintf(stderr, "Error: Incorrect input"
                                                " pattern for regioncount\n");
                                        print_usage();
                                        assert(FALSE);
                                }

                                break;
                        /* regioncount_low */
                        case 'i':
                                set_low_high_incr(REGEX_NUMBERS, &pios_args->N,
                                                  optarg, FLAG_LOW, &flag_regioncount,
                                                  "regioncount_low");
                                break;
                        /* regioncount_high */
                        case 'j':
                                set_low_high_incr(REGEX_NUMBERS, &pios_args->N,
                                                 optarg, FLAG_HIGH, &flag_regioncount,
                                                 "regioncount_high");
                                break;
                        /* regioncount_inc */
                        case 'k':
                                set_low_high_incr(REGEX_NUMBERS, &pios_args->N, 
                                                 optarg, FLAG_INCR, &flag_regioncount, 
                                                 "regioncount_incr");
                                break;

                        /* offset */
                        case 'o':
                                flag_offset |= 1;
                                if (regex_match(optarg, REGEX_SIZE)) { 
                                        pios_args->O.val[0] = 
                                        pios_interpret_KMGT(optarg);

                                        pios_args->O.val_count = 1;
                                } else if(split_string(optarg, 
                                                     REGEX_SIZE_COMMA, 
                                                     &pios_args->O) == -1) {
                                        fprintf(stderr, "Error: Incorrect input"
                                                " pattern for offset\n");
                                        print_usage();
                                        assert(FALSE);
                                }

                                break;
                        /* offset_low */
                        case 'm':
                                pios_args->O.val_count = 0;
                                set_low_high_incr(REGEX_SIZE,
                                                  &pios_args->O, optarg, FLAG_LOW,
                                                  &flag_offset, "offset_low");
                                break;
                        /* offset_high */
                        case 'q':
                                set_low_high_incr(REGEX_SIZE, &pios_args->O,
                                                  optarg, FLAG_HIGH, &flag_offset,
                                                  "offset_high");
                                break;
                        /* offset_inc */
                        case 'r':
                                set_low_high_incr(REGEX_NUMBERS, &pios_args->O,
                                                  optarg, FLAG_INCR, &flag_offset,
                                                  "offset_incr");
                                break;

                        /* chunksize */
                        case 'c':
                                flag_chunksize |= 1;
                                if (regex_match(optarg, REGEX_SIZE)) {
                                        pios_args->C.val[0] = 
                                        pios_interpret_KMGT(optarg);
                                        pios_args->C.val_count = 1;
                                } else if(split_string(optarg, REGEX_SIZE_COMMA,
                                                     &pios_args->C) == -1) {
                                        fprintf(stderr, "Error: Incorrect input"
                                                " pattern for chunksize\n");
                                        print_usage();
                                        assert(FALSE);
                                }

                                break;
                        /* chunksize_low */
                        case 'a':
                                set_low_high_incr(REGEX_SIZE, &pios_args->C,
                                                  optarg, FLAG_LOW, &flag_chunksize,
                                                  "chunksize_low");
                                break;
                        /* chunksize_high */
                        case 'b':
                                set_low_high_incr(REGEX_SIZE, &pios_args->C,
                                                  optarg, FLAG_HIGH, &flag_chunksize,
                                                  "chunksize_high");
                                break;
                        /* chunksize_inc */
                        case 'g':
                                set_low_high_incr(REGEX_NUMBERS, &pios_args->C,
                                                  optarg, FLAG_INCR, &flag_chunksize,
                                                  "chunksize_incr");
                                break;

                        /* regionsize */
                        case 's':
                                flag_regionsize |= 1;
                                if (regex_match(optarg, REGEX_SIZE)) {
                                        pios_args->S.val[0] =
                                        pios_interpret_KMGT(optarg);
                                        pios_args->S.val_count = 1;
                                } else if (split_string(optarg, REGEX_SIZE_COMMA,
                                                     &pios_args->S) == -1) {
                                        fprintf(stderr, "Error: Incorrect input"
                                                " pattern for regionsize\n");
                                        print_usage();
                                        assert(FALSE);
                                }

                                break;
                        /* regionsize_low */
                        case 'A':
                                set_low_high_incr(REGEX_SIZE, &pios_args->S,
                                                  optarg, FLAG_LOW, &flag_regionsize,
                                                  "regionsize_low");
                                break;
                        /* regionsize_high */
                        case 'B':
                                set_low_high_incr(REGEX_SIZE, &pios_args->S,
                                                  optarg, FLAG_HIGH, &flag_regionsize,
                                                  "regionsize_high");
                                break;
                        /* regionsize_inc */
                        case 'C':
                                set_low_high_incr(REGEX_NUMBERS, &pios_args->S,
                                                  optarg, FLAG_INCR, &flag_regionsize,
                                                  "regionsize_incr");
                                break;
                        /* load */
                        case 'L':
                                parse_load_params(optarg, pios_args); 
                                break;
                        /* path */
                        case 'p':
                                pios_args->path = optarg;
                                break;
                        /* cleanup */
                        case 'x':
                                pios_args->cleanup = 1;
                                break;
                        /* Pre Run command */
                        case 'P':
                                pios_args->prerun = optarg;
                                break;
                        /* Post Run command */
                        case 'R':
                                pios_args->postrun = optarg;
                                break;
                        /* regionnoise */
                        case 'I':
                                if (regex_match(optarg, REGEX_NUMBERS)) 
                                        pios_args->regionnoise = 
                                        pios_interpret_KMGT(optarg);
                                else {
                                        fprintf(stderr, "Error: Incorrect input"
                                                " pattern for regionnoise\n");
                                        print_usage();
                                        assert(FALSE);
                                }
                                
                                break;
                        /* chunknoise */
                        case 'N':
                                if (regex_match(optarg, REGEX_SIZE)) { 
                                        pios_args->chunknoise = 
                                        pios_interpret_KMGT(optarg);
                                } else {
                                        fprintf(stderr, "Error: Incorrect input"
                                                " pattern for chunknoise\n");
                                        print_usage();
                                        assert(FALSE);
                                }
                                break;
                        /* thread delay */
                        case 'T':
                                if (regex_match(optarg, REGEX_NUMBERS)) 
                                        pios_args->thread_delay = 
                                        pios_interpret_KMGT(optarg);
                                else {
                                        fprintf(stderr, "Error: Incorrect input"
                                                " pattern for threaddelay\n");
                                        print_usage();
                                        assert(FALSE);
                                }
                                break;
                        /* verify */
                        case 'V':
                                if (argv[optind] == NULL)
                                        pios_args->V.val_count = -1;
                                else if (argv[optind][0] == '-')
                                        pios_args->V.val_count = -1;
                                else if (regex_match((const char *)argv[optind ], REGEX_NUMBERS)) { 
                                        pios_args->V.val_count = 1;
                                        pios_args->V.val[0] = 
                                        pios_interpret_KMGT(argv[optind]);
                                }
                                else if(split_string((const char *)argv[optind], 
                                                      REGEX_NUMBERS_COMMA, &pios_args->V) == -1) {
                                        fprintf(stderr, "Error: Incorrect input"
                                                " pattern for verify.\n");
                                        print_usage();
                                        assert(FALSE);
                                }
                                break;
                        case '?':
                                print_usage();
                                assert(FALSE);
                        default:
                                print_usage();
                                assert(FALSE);
                }

        check_mutual_exclusive_command_lines(flag_thread, "threadcount");
        check_mutual_exclusive_command_lines(flag_regioncount, "regioncount");
        check_mutual_exclusive_command_lines(flag_offset, "offset");
        check_mutual_exclusive_command_lines(flag_regionsize, "regionsize");
        check_mutual_exclusive_command_lines(flag_chunksize, "chunksize");

        if(pios_args->path == NULL) {
                printf("Error: Path missing\n");
                print_usage();
                assert(FALSE);
        }
        return;
}

int check_device_size(__u64 device_size, __u64 size)
{

        if (device_size < size) {
                fprintf(stderr, "Error: Device doesn't have sufficient free "
                        "space.\n");
                assert(FALSE);
        }
        return 0;
}

void set_actual_streams(struct run_arg *runarg, int fd, __u64 device_size)
{
        int n;
        __u64 total_test_size;

        for (n = 0; n < runarg->regioncount; n++) {
                /* setting up stream[] */
                runarg->stream[n].fd = fd;
                runarg->stream[n].offset = runarg->offset * n;
                runarg->stream[n].max_offset = runarg->offset * n +
                                              runarg->regionsize;
        }
        total_test_size = runarg->stream[n-1].offset +
                          runarg->regionsize;
        /* Adding 10% to total test size */
        total_test_size = total_test_size + 
                          (double)(total_test_size) * (double) (0.1);
        if (!runarg->verify)
                check_device_size(device_size, total_test_size);
}

/* set file handle, offset and size for each region's stream */
void setup_streams(struct run_arg *runarg, struct pios_args *pios_args,
                   int runno)
{
        struct stat64 statbuf;
        int fd;
        int n;
        struct statvfs64 pios_statvfs;
        __u64 device_size = 0ULL;
        char path[4096];

        int flags = 0;

        if (pios_args->io_type & DMUIO) {
#ifndef ENABLE_DMU
                assert(FALSE);
#endif
                /* ZFS DMU mode */
                udmu_objset_t uos;
                uint64_t object = 0ULL;
                int error;
                char *pool = strdup(pios_args->path);

                udmu_init();

                error = udmu_open_objset(pool, &uos);
                if (error) {
                        fprintf(stderr, "error opening pool(%s): %s",
                                pios_args->path, strerror(errno));
                        assert(FALSE);                        
                }

                if (!pios_args->fpp) {
                        object = udmu_object_create(uos);
                        assert(object);
                }

                for (n = 0; n < runarg->regioncount; n++) {
                        if (pios_args->fpp) {
                                /* File per process */
                                runarg->stream[n].obj.uos = uos;
                                runarg->stream[n].obj.object =
                                        udmu_object_create(uos);
                                runarg->stream[n].offset = runarg->offset;
                                runarg->stream[n].max_offset = 
                                        runarg->offset + runarg->regionsize;
                        } else {
                                /* Single shared file */
                                runarg->stream[n].obj.uos = uos;
                                runarg->stream[n].obj.object = object;
                                runarg->stream[n].offset = runarg->offset * n;
                                runarg->stream[n].max_offset =
                                        runarg->offset * n + runarg->regionsize;
                        }
                }
                free (pool);
                return;
        }

#if defined (linux)
        if(pios_args->io_type & DIRECTIO)
                flags = O_DIRECT;
#endif
        if (stat64(pios_args->path, &statbuf)) {
                fprintf(stderr, "path: %s, error %s\n", pios_args->path,
                strerror(errno));
                assert(FALSE);
        }

        /* given path is directory and N files need to be created. */
        if (S_ISDIR(statbuf.st_mode) && pios_args->fpp) {
                char path[4096];
                /* N files will be created and each will have regionsize of 
                 * data written in chunksize writes at offset */
                for (n = 0; n < runarg->regioncount; n++) {
                        sprintf(path, "%s/r%d-f%d", pios_args->path, runno, n);

                        fd = open64(path, O_RDWR | O_CREAT | flags, 0600);
                        if (fd < 0) {
                                fprintf(stderr, "cannot open %s: %s\n",
                                        path, strerror(errno));
                                assert(FALSE);
                        }
                        runarg->stream[n].fd = fd;
                        runarg->stream[n].offset = runarg->offset;
                        runarg->stream[n].max_offset = 
                        runarg->offset + runarg->regionsize;
                }
                if (statvfs64(path, &pios_statvfs) != 0) {
                        fprintf(stderr, "Couldn't do statvfs on %s\n", path);
                        assert(FALSE);
                }
                device_size = pios_statvfs.f_bavail * pios_statvfs.f_bsize;
                check_device_size(device_size, runarg->stream[n-1].offset +
                                  runarg->regionsize);
                return;
        } /* given path is directory and N files need to be created. */

        /* if given path is DIR and data needs to be written at N locations */
        if (S_ISDIR(statbuf.st_mode) && !pios_args->fpp) {
                /* pathname: <directory-path>/<runno> */
                sprintf(path, "%s/r%d", pios_args->path, runno);
                fd = open64(path, O_RDWR | O_CREAT | flags, 0600);
                if (fd < 0) {
                        fprintf(stderr, "cannot open %s: %s\n",
                                path, strerror(errno));
                        assert(FALSE);
                }
                if (statvfs64(path, &pios_statvfs) != 0) {
                        fprintf(stderr, "Couldn't do statvfs on %s\n", path);
                        assert(FALSE);
                }
                device_size = pios_statvfs.f_bavail * pios_statvfs.f_bsize;
                set_actual_streams(runarg, fd, device_size);

        } else if (S_ISBLK(statbuf.st_mode)) { /* given path is block device */
                is_block_device = 1; /* will be used in cleanup_files() */
                if (pios_args->io_type & COWIO) {
                        printf("\nError: Shadow copy option (--cowio) can not "
                                        "be specified for block devices.\n");
                        assert(FALSE);
                }
                fd = open64(pios_args->path, O_RDWR);
                if (fd < 0) {
                        fprintf(stderr, "cannot open %s: %s\n",
                                pios_args->path, strerror(errno));
                        assert(FALSE);
                }
# if defined BLKGETSIZE64       /* in sys/mount.h */
                ioctl(fd, BLKGETSIZE64, &device_size);
# endif
                set_actual_streams(runarg, fd, device_size);

        } else {
                fprintf(stderr, "wrong file type\n");
                assert(FALSE);
        }

        /* check the device size for data written in test */
        return;
}

/* this function:
 *  - sets up the arguments for one run
 *  - creates files as needed
 *  - initializes the file streams for use by the thread */
void setup_run_arg(int runno, struct run_arg **runarg,
                   struct thread_data ***th_data, __u64 T, __u64 N, __u64 C,
                   __u64 S, __u64 O, struct pios_args *pios_args)
{
        int i;

        *runarg = pios_malloc(sizeof(**runarg) + N * 
                              sizeof(struct pios_stream));
        assert(*runarg);
        memset(*runarg, 0, sizeof(**runarg) + N * sizeof(struct pios_stream));

        (*runarg)->runno = runno;
        (*runarg)->threadcount = T;
        (*runarg)->regioncount = N;
        (*runarg)->chunksize = C;
        (*runarg)->regionsize = S;
        (*runarg)->offset = O;
        (*runarg)->thread_delay = pios_args->thread_delay;
        (*runarg)->chunknoise = pios_args->chunknoise;
        (*runarg)->verify = pios_args->V.val_count;
        (*runarg)->io_type = pios_args->io_type;
        (*runarg)->regionnoise = pios_args->regionnoise;
        (*runarg)->path = pios_args->path;

        setup_streams(*runarg, pios_args, runno);

        memset(&work_item_control, 0, sizeof(work_item_control));

        *th_data = pios_malloc(sizeof(**th_data) * T);
        assert(*th_data);

        for (i=0; i<(*runarg)->threadcount; i++) {
                struct thread_data *th_datap;
                th_datap = pios_malloc(sizeof(*th_datap));
                assert(th_datap);
                memset(th_datap, 0, sizeof(*th_datap));
                (*th_data)[i] = th_datap;
                th_datap->arg = *runarg;
                th_datap->thread_no = i;
        }
}

int get_work_item(struct run_arg *args, struct work_item_control *ctl,
                  int *fd, udmu_obj_t *obj,__u64 *offset, __u32 *chunksize,
                  pthread_mutex_t *lock, __u32 *pios_regioncount,
                  int regionnoise)
{
        int rc;
        int i, count = 0;
        int temp_regionnoise = 0;
        struct timeval rand_seed_time;

        rc = pthread_mutex_lock(lock);
        assert(rc == FALSE);
        i = ctl->next_region;
        while (count < args->regioncount) {
                rc = i % args->regioncount;

                /* test if region is fully written */
                if (args->stream[rc].offset + args->chunksize >
                    args->stream[rc].max_offset) {
                        i++;
                        count++;
                        continue;
                }

                /* found a region that still needs work
                   add random stuff to chunksize */
                *offset = args->stream[rc].offset;
                *chunksize = args->chunksize;
                *fd = args->stream[rc].fd;
                *obj = args->stream[rc].obj;
                *pios_regioncount = rc;
                args->stream[rc].offset += *chunksize;

                /* update ctl structure */
                if(regionnoise) {
                        gettimeofday(&rand_seed_time, NULL);
                        srand(rand_seed_time.tv_usec);
                        temp_regionnoise = rand();
                        temp_regionnoise = temp_regionnoise % regionnoise;
                        ctl->next_region += temp_regionnoise;
                } else {
                        ctl->next_region++;
                }
                pthread_mutex_unlock(lock);
                return 1;
        }
        /* nothing left to do */
        pthread_mutex_unlock(lock);
        return 0;
}

/* the main function of each thread is to get work items and do
 * IO until there is nothing left to do */
void *thread_main(void *data)
{
        struct thread_data *th_data = data;
        struct run_arg *arg = th_data->arg;
	int thrno = th_data->thread_no;
        int io_type = arg->io_type;

        int thread_delay = arg->thread_delay;
        int temp_thread_delay;

        int chunknoise = arg->chunknoise;
        int temp_chunknoise;

        time_t timestamp = arg->timestamp;

        int verify = arg->verify;

        int regionnoise = arg->regionnoise;
        __u64 file_boundry = 0ULL;

        int runno = arg->runno;
#if defined(__sun__) || defined(__sun)
        __u64 data_boundary;
#endif

	struct stat64 statbuf;
        struct stat64 logbuf;
        int fd = 0;
        int rc = 0;
        int logfd = 0;
        int error;
        udmu_obj_t obj;
#if defined(linux)
        int   blocksize, logical_blk;
#endif
	char LOGFILE[25];
        __u32 chunk;
        __u64 offset;

        /* enlarge to deal with random noise */
        struct timeval pios_time_start, pios_time_end;
        struct timeval rand_seed_time;
        __u32 pios_regioncount;
        void *buff;
        void *rdbuf = NULL;
        void *verify_read_buffer;
        int ret;

        /* Adding chunknoise */
        /* random noise will be added in the range of -chunknoise to chunknoise */
        if(chunknoise == FALSE)
                temp_chunknoise = 0;
        else {
                gettimeofday(&rand_seed_time, NULL);
                srand(rand_seed_time.tv_usec);
                temp_chunknoise = rand();
                temp_chunknoise = temp_chunknoise % (chunknoise * 2);
                temp_chunknoise = temp_chunknoise - chunknoise;
        }

#if defined (linux)
        if(io_type & DIRECTIO) {
                buff = memalign(sysconf(_SC_PAGESIZE),arg->chunksize +
                                temp_chunknoise);
                if (buff == NULL) {
                        fprintf(stderr, "memalign failed\n");
                        assert(FALSE);
                }
        }
        else 
#endif
                buff = (void *)pios_malloc(arg->chunksize + temp_chunknoise);
                assert(buff);
                memset(buff, 0, (arg->chunksize + temp_chunknoise));
        /* As --chunknoise is random there is no point in adding it here, as 
         * --verify will always fail with --chunknoise */
#if defined (linux)
        if(io_type & DIRECTIO) {
                verify_read_buffer = memalign(sysconf(_SC_PAGESIZE),
                                              arg->chunksize);
                if (verify_read_buffer == NULL) {
                        fprintf(stderr, "memalign failed\n");
                        assert(FALSE);
                }
        }
        else 
#endif
        verify_read_buffer = (void *)pios_malloc(arg->chunksize);
        assert(verify_read_buffer);
        memset(verify_read_buffer, 0, arg->chunksize);

        if (io_type & COWIO) {
                sprintf(LOGFILE, "%s/shadow.%d", arg->path, thrno);
                logfd = open64(LOGFILE, O_RDWR|O_APPEND|O_CREAT, 
                               S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
                if (logfd < 0) {
                        fprintf(stderr, "cannot open %s: %s\n", LOGFILE,
                                strerror(errno));
                        assert(FALSE);
                }
                if (fstat64(logfd, &logbuf) < 0) {
                        fprintf(stderr, "Can not stat the log file, aborting.");
                        assert(FALSE);
                }
                /* Allocate the read buffer for shadow copy aligned to block size. If
                 * it is not already aligned, padd it with zeroes.
                 */
                if ((arg->chunksize + temp_chunknoise) % logbuf.st_blksize != 0)
                        file_boundry = (arg->chunksize + temp_chunknoise) +
                                       (logbuf.st_blksize - ((arg->chunksize +
                                                              temp_chunknoise) %
                                                            logbuf.st_blksize));
                else
                        file_boundry = (arg->chunksize + temp_chunknoise);

#if defined (linux)
                if(io_type & DIRECTIO) {
                        rdbuf = memalign(sysconf(_SC_PAGESIZE),file_boundry);
                        if (rdbuf == NULL) {
                                fprintf(stderr, "memalign failed for rdbuf\n");
                                assert(FALSE);
                        }
                }
                else 
#endif
                        rdbuf = pios_malloc(file_boundry);

                memset(rdbuf, 0, file_boundry);
        }

        while (get_work_item(arg, &work_item_control, &fd, &obj, &offset,
               &chunk, &lock, &pios_regioncount, regionnoise)){
                if (verify != 0) {
                        timestamp = 0;
                        rc = pthread_mutex_lock(&lock_timestamp);
                        assert(rc == FALSE);
                        if(main_timestamp == 0 && verify == -1) {
                                        /* to make sure that first offset 
                                         * always remain the same 
                                         * as multiple threads may modify it */
                                if (io_type & DMUIO) {
                                        error = udmu_read(obj.uos, obj.object, offset,
                                                          chunk, verify_read_buffer);
                                } else {
                                        rc = pread64(fd,
                                                     (void*)verify_read_buffer,
                                                     chunk, offset);
                                        memcpy(&timestamp, verify_read_buffer,
                                               sizeof(int));
                                        main_timestamp = timestamp;
                                }
                        }
                        timestamp_of_run = main_timestamp;
                        pthread_mutex_unlock(&lock_timestamp);
                        /* reading whole chunk here and then taking out 
                         * timestamp from it in next step */
                        gettimeofday(&pios_time_start, NULL);
                        if (io_type & DMUIO) {
                                error = udmu_read(obj.uos, obj.object, offset,
                                                  chunk, verify_read_buffer);
                                if (error) {
                                        rc = 0;
                                } else {
                                        rc = chunk;
                                }
                        } else {
                                rc = pread64(fd, (void*)verify_read_buffer, chunk, 
                                             offset);
                        }
                        memcpy(&timestamp, verify_read_buffer,sizeof(int));
                        gettimeofday(&pios_time_end, NULL);
                        if(timestamp != main_timestamp || timestamp == 0)
                                fprintf(stderr,"Run: %d, Timestamp Check: "
                                        "Failed at offset: %llu\n", runno, 
                                        offset);

                        rc = pthread_mutex_lock(&lock_time_region);
                        assert(rc == FALSE);
                        chunks_read_write++;
                        *time_per_region[pios_regioncount] += 
                                (pios_time_end.tv_sec + 
                                        (pios_time_end.tv_usec/1000000.0)) -
                                (pios_time_start.tv_sec + 
                                        (pios_time_start.tv_usec/1000000.0));
                        pthread_mutex_unlock(&lock_time_region);

                } else { 
                        memcpy(buff, &timestamp, sizeof(time_t));

                        if(thread_delay == FALSE) {
                                temp_thread_delay = 0;
                        } else {
                                /* Adding thread delay */
                                gettimeofday(&rand_seed_time, NULL);
                                srand(rand_seed_time.tv_usec);
                                temp_thread_delay = rand();
                                temp_thread_delay = temp_thread_delay %
                                    thread_delay;
                                usleep(temp_thread_delay * 1000);
                        }

                        /* COWIO = 1
                         * DIRECTIO = 2
                         * POSIXIO = 0
                         * DMUIO = 4
                         */
                        if (io_type & COWIO) {
        	                gettimeofday(&pios_time_start, NULL);
                                if ((fstat64(fd, &statbuf)) < 0) {
                                        fprintf(stderr, "Can not stat "
                                                "data file, aborting.");
                                        assert(FALSE);
                                }

                                /* Calculate if the physical block at this
                                 * offset is allocated. If yes, we need to
                                 * make shadow copy of the existing data.
                                 */
#if defined(linux)
                                assert(ioctl(fd, FIGETBSZ, &blocksize) == 0);
                                logical_blk = (offset + blocksize - 1) / 
                                              blocksize;
                                ioctl(fd, FIBMAP, &logical_blk);
                                if (logical_blk > 0)
                                {
#elif defined(__sun__) || defined(__sun)
                                if (statbuf.st_size > offset +
                                        (chunk + temp_chunknoise)) {
                                        data_boundary = offset + (chunk + 
                                                temp_chunknoise);
                                } else {
                                        data_boundary = statbuf.st_size;
                                }

                                if (offset < statbuf.st_size ||
                                   (lseek(fd, offset, SEEK_DATA) <= 
                                    data_boundary)) {
#endif
                                        rc = pread64(fd, (void*)rdbuf, 
                                                     (chunk + temp_chunknoise),
                                                     offset);
                                        if (rc != (chunk + temp_chunknoise) &&
                                           (offset + rc < statbuf.st_size)) {
                                                fprintf(stderr, 
                                                        "IO error, aborting\n");
                                                assert(FALSE);
                                        }
                                        lseek(logfd, 0, SEEK_END);
                                        rc = write(logfd, rdbuf, file_boundry);
                                }
                        }
                        if(!(io_type & COWIO)) {
                                gettimeofday(&pios_time_start, NULL);
                        }

                        
                        if (io_type & DMUIO) {
                                error = udmu_write(obj.uos, obj.object, offset,
                                           (chunk  + temp_chunknoise), buff);
                                if (error) {
                                        fprintf(stderr, "Error: IO error while"
                                                " doing udmu_write(), aborting."
                                                " - %d\n", error);
                                        assert(FALSE);
                                } else {
                                        rc = (chunk  + temp_chunknoise);
                                }
                        } else {
                                rc = pwrite64(fd, (void*)buff,
                                              (chunk  + temp_chunknoise),
                                              offset);
                                if (rc != (chunk + temp_chunknoise)) {
                                        fprintf(stderr, "Error: IO error while"
                                                " doing pwrite64, aborting.\n");
                                        assert(FALSE);
                                }
                        }
                        gettimeofday(&pios_time_end, NULL);

                        pthread_mutex_lock(&lock_data_written);

                        if(chunknoise)
                                data_written += rc;
                        
                        pthread_mutex_unlock(&lock_data_written);

                        rc = pthread_mutex_lock(&lock_time_region);
                        assert(rc == FALSE);
                        chunks_read_write++;

                        *time_per_region[pios_regioncount] += 
                                (pios_time_end.tv_sec + 
                                 (pios_time_end.tv_usec/1000000.0)) -
                                (pios_time_start.tv_sec + 
                                 (pios_time_start.tv_usec/1000000.0));
                        pthread_mutex_unlock(&lock_time_region);

                }
        }

        free(buff);
        if(io_type & COWIO) {
                free(rdbuf);
                close(logfd);
        }

        ret = pthread_barrier_wait (barrier);
        if ((ret != 0) && (ret != PTHREAD_BARRIER_SERIAL_THREAD)) {
                fprintf (stderr, "Error when waiting for barrier\n");
        }
        return(0);
}

/* cleanup threads, and barrier */
void cleanup_threads(struct run_arg *arg, struct thread_data **th_data)
{
        int ret;
        int i;

        ret = pthread_barrier_wait(barrier);
        if ((ret != 0) && (ret != PTHREAD_BARRIER_SERIAL_THREAD))
                fprintf (stderr, "Error when waiting for barrier\n");
        for (i = 0; i < arg->threadcount; i++) {
                if (pthread_join(thread[i], NULL) != 0) {
                        printf ("Error when joining thread\n");
                        assert(FALSE);
                }
        }

        if (pthread_barrier_destroy(barrier) != 0) {
                printf ("Error when destroying barrier\n");
                assert(FALSE);
        }
}

/* This function starts all the threads for a run and waits for
 * their completion using a barrier */
void start_threads(struct run_arg *arg, struct thread_data **th_data, 
                   time_t timestamp)
{
        int i;

	time_per_region = pios_malloc(sizeof(double) * arg->regioncount);	
        memset(time_per_region, 0, sizeof(double) * arg->regioncount);
        barrier = pios_malloc(sizeof(pthread_barrier_t));
	for (i=0; i< arg->regioncount; i++) {
		time_per_region[i] = pios_malloc(sizeof(double));
                memset(time_per_region[i],0, sizeof(double));
	}
        
        if (pthread_barrier_init(barrier, NULL, arg->threadcount + 1) != 0) {
                printf ("Error: While initializing barrier.\n");
                assert(FALSE);
        }

        thread = pios_malloc(sizeof(pthread_t) * arg->threadcount);

        for (i = 0; i < arg->threadcount; i++) {
                th_data[i]->arg->timestamp = timestamp;
                if (pthread_create(&thread[i],NULL, &thread_main, 
                   th_data[i]) != 0) {
                        fprintf(stderr, "Error: Couldn't create %d threads.",
                                arg->threadcount);
                        fprintf(stderr, " %s.\n", strerror(errno));
                        assert(FALSE);
                }
        }
        return;
}

/* cleans up run_arg structure and thread_data structure used */
void cleanup_run_arg(struct run_arg *arg, struct thread_data ***th_data)
{
        int i;
        
        assert(*th_data);
        for (i = 0; i < arg->threadcount; i++) {
                free((*th_data)[i]);
        }
        free(*th_data);

        free(thread);
        free(arg);

}

void cleanup_files(struct pios_args *pios_args)
{
        int status;
        char cmd[128];
        if(is_block_device)
                return;

        sprintf(cmd, "rm -rf %s/r*", pios_args->path);
        status = system(cmd);
        if(status) {
                fprintf(stderr,"Error: %s failed.\n", cmd);
                assert(FALSE);
        }
        return;
}

void do_one_run(int runno, __u64 T, __u64 N, __u64 C, __u64 S, __u64 O,
                struct pios_args *pios_args)
{
        /*      __u32 timestamp = getimeofday();
        *       struct pios_stream *streams; */
        struct run_arg *runarg;
        struct thread_data **th_data;
	struct timeval pios_init_time, pios_last_time;
#if defined(__sun__) || defined(__sun)
	// int rc;
#endif

        setup_run_arg(runno, &runarg, &th_data, T, N, C, S, O, pios_args);
        /* call_script(pre_run, timestamp); */
        //rc = system("/bin/sh ./pre_run.sh &");
        gettimeofday(&pios_init_time, NULL);
        start_threads(runarg, th_data, pios_init_time.tv_sec);
        /* call_script(post_run, timestamp); */
        //rc = system("/bin/sh ./post_run.sh &");
        cleanup_threads(runarg, th_data);
        gettimeofday(&pios_last_time, NULL);

        real_time = (pios_last_time.tv_sec + 
                     (pios_last_time.tv_usec/1000000.0)) - 
        (pios_init_time.tv_sec + (pios_init_time.tv_usec/1000000.0));

        /* if --cleanup flag is given we need to remove files after each run */
        if (pios_args->cleanup)
                cleanup_files(pios_args);
        if (pios_args->V.val_count == 0)
                timestamp_of_run = pios_init_time.tv_sec;
        print_stats(runarg, timestamp_of_run);
        cleanup_run_arg(runarg, &th_data);
}

int get_next(__u64 *val, struct pios_range_repeat *pios_range) 
{
        /* if low, incr, high is given */
        if (pios_range->val_count == 0) {
                *val = pios_range->val_low + 
                       (double)(pios_range->val_low * 
                                ((double)(pios_range->next_val) /
                                 (double)(100)));
                if(*val > pios_range->val_high)
                        return(0); /* No more values now as limit is exceeded*/
                if(!pios_range->next_val)
                        pios_range->next_val = pios_range->val_inc_perc;
                else 
                        pios_range->next_val = pios_range->next_val +  
                                               pios_range->val_inc_perc;
                return(1);/* more values to come */

        /* if only one val is given */
        } else if (pios_range->val_count == TRUE) {
                if(pios_range->next_val)
                        return(0); /* No more values now as we have only one 
                                    * value*/
                *val = pios_range->val[0];
                pios_range->next_val++;
                return(1);/* more values to come */
        /* if comma separated values are given */
        } else if (pios_range->val_count > 1) {
                if( pios_range->next_val > pios_range->val_count - 1 )
                        return 0; /* No more values now as no of values given 
                                   * are exceeded*/
                *val = pios_range->val[pios_range->next_val];
                pios_range->next_val++;
                return 1; /* more values to come */
        }
        return 0;
}

void error_off_less()
{
        fprintf(stderr, "Error: in any run offset can not be smaler than "
                "regionsize or chunksize.\n");
        return;

}

void get_next_ts(__u32 next, __u32 count, __u64 val)
{
        if(count >= 1) {
                main_timestamp = val;
                if(next > (count - 1)) {
                        fprintf(stderr, "Error: Too few timestamps with "
                                "--verify. Though verified given timestamps"
                                ".\n");
                        exit(0);
               }
        }
        return;
}

void error_chunk_less()
{
        fprintf(stderr, "Error: in any run chunksize can not be smaller than"
                " regionsize.\n"); 
        return;
}

void do_all_runs(struct pios_args *pios_args)
{
        __u64 T = 0, N = 0, C = 0, S = 0, O = 0;
        int runno = 1;
        __u32 next;

        while (get_next(&T, &pios_args->T)) {
                while (get_next(&C, &pios_args->C)) {
                        while (get_next(&S, &pios_args->S)) {
                               if (S < C) {
                                        error_chunk_less();
                                        assert(0);
                                }
                                while (get_next(&N, &pios_args->N)) {
                                        while (get_next(&O, &pios_args->O)) {
                                                if(O < S || O < C) {
                                                        error_off_less();
                                                        assert(0);
                                                }
                                                next = pios_args->V.next_val;
                                                get_next_ts(
                                                        next,
                                                        pios_args->V.val_count,
                                                        pios_args->V.val[next]);
                                                pios_args->V.next_val++;
                                        do_one_run(runno, T, N, C, S, O,
                                                   pios_args);
                                        runno++;
                                        }
                                        pios_args->O.next_val = 0;
                                }
                                pios_args->N.next_val = 0;
                        }
                        pios_args->S.next_val = 0;
                }
                pios_args->C.next_val = 0;
        }
        return;
}

void setrlimit_for_nofiles(struct pios_args *pios_args)
{
        struct rlimit rlim;
        long limit = 0;

        getrlimit(RLIMIT_NOFILE, &rlim);

        if(pios_args->N.val_count == 0)
                limit = pios_args->N.val_high;
        else if (pios_args->N.val_count == 1)
                limit = pios_args->N.val[0]; 
        else if (pios_args->N.val_count > 1)
                limit = pios_args->N.val[pios_args->N.val_count];

        if(pios_args->fpp && limit > (rlim.rlim_cur-10)) {
                fprintf(stderr, "Currnt limit of open files is %lu and pios "
                        "wants: %llu.\n", rlim.rlim_cur,
                        pios_args->N.val[0] + 10);
                rlim.rlim_cur = pios_args->N.val[0] + 10000;
                rlim.rlim_max = pios_args->N.val[0] + 10000;
                fprintf(stderr, "Setting soft limit of no of open files to: %lu"
                        " and Hard Limit to %lu, continuing ...\n\n", 
                        rlim.rlim_cur, rlim.rlim_max);
                if(setrlimit(RLIMIT_NOFILE, &rlim) == -1) {
                        fprintf(stderr, "setrlimit failed for no of files "
                                "specified\n");
                        assert(0);
                }
        }
}

int main(int argc, char **argv)
{
        struct pios_args pios_args;
        memset(&pios_args, 0, sizeof(pios_args));

        interpret_args(argc, argv, &pios_args); /* fill in pios_args */
        setrlimit_for_nofiles(&pios_args);
                

        if(pios_args.V.val_count == -1)
                printf("\nVerifying timestamps written in previous runs by "
                       "comparing first timestamp found with others...\n"
                       "Following stats are of read performance.\n\n");
        else if (pios_args.V.val_count >= 1)
                printf("\nVerifying timestamps written in previous runs by"
                       " comparing them with timestamps given on command "
                       "line...\nFollowing stats are of read performance.\n\n");
        else
                printf("\nFollowing stats are of write performance.\n\n");


        printf("Run\tTimestamp\tT\tN\tS\tC\tAggregate throughput\tlowest time\t"
               "\tHighest time\t\tRun time\n");
        printf("---------------------------------------------------------------"
               "-----------------------\n");
        do_all_runs(&pios_args);
        printf("--------------------------------------------------------------"
               "----------------------------------------\n");
        fprintf(stderr, "Total no of chunks read,write: %llu\n", 
                chunks_read_write);
        return 0;
}

