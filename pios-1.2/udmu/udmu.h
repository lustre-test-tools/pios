/* -*- mode: c; c-basic-offset: 8; indent-tabs-mode: nil; -*-
 * vim:expandtab:shiftwidth=8:tabstop=8:
 *
 *  Copyright (c) 2006 Cluster File Systems, Inc.
 *   Author: Manoj Joseph <manoj@clusterfs.com>
 *
 *   This file is part of the Lustre file system, http://www.lustre.org
 *   Lustre is a trademark of Cluster File Systems, Inc.
 *
 *   You may have signed or agreed to another license before downloading
 *   this software.  If so, you are bound by the terms and conditions
 *   of that agreement, and the following does not apply to you.  See the
 *   LICENSE file included with this distribution for more information.
 *
 *   If you did not agree to a different license, then this copy of Lustre
 *   is open source software; you can redistribute it and/or modify it
 *   under the terms of version 2 of the GNU General Public License as
 *   published by the Free Software Foundation.
 *
 *   In either case, Lustre is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   license text for more details.
 */

#ifndef	_UDMU_H
#define	_UDMU_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <sys/types.h>

struct objset;
struct zilog;


typedef struct udmu_objset {
	struct objset *os;
	struct zilog *zilog;
} udmu_objset_t;

typedef struct udmu_obj {
	udmu_objset_t uos;
        uint64_t object;
} udmu_obj_t;

extern void udmu_init();

extern void udmu_debug(int level);

extern int udmu_open_objset(char *osname, udmu_objset_t *uos);

extern void udmu_close_objset(udmu_objset_t uos);

extern uint64_t udmu_object_create(udmu_objset_t uos);

extern int udmu_object_open(udmu_objset_t uos, uint64_t object);

extern int udmu_read(udmu_objset_t uos, uint64_t object, uint64_t offset,
		     uint64_t size, void *buf);

extern int udmu_write(udmu_objset_t uos, uint64_t object, uint64_t offset,
	       uint64_t size, const void *buf);

extern void udmu_sync(udmu_objset_t uos);

#ifdef	__cplusplus
}
#endif

#endif /* _UDMU_H */
