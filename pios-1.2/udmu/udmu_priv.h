/* -*- mode: c; c-basic-offset: 8; indent-tabs-mode: nil; -*-
 * vim:expandtab:shiftwidth=8:tabstop=8:
 *
 *  Copyright (c) 2006 Cluster File Systems, Inc.
 *   Author: Manoj Joseph <manoj@clusterfs.com>
 *
 *   This file is part of the Lustre file system, http://www.lustre.org
 *   Lustre is a trademark of Cluster File Systems, Inc.
 *
 *   You may have signed or agreed to another license before downloading
 *   this software.  If so, you are bound by the terms and conditions
 *   of that agreement, and the following does not apply to you.  See the
 *   LICENSE file included with this distribution for more information.
 *
 *   If you did not agree to a different license, then this copy of Lustre
 *   is open source software; you can redistribute it and/or modify it
 *   under the terms of version 2 of the GNU General Public License as
 *   published by the Free Software Foundation.
 *
 *   In either case, Lustre is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   license text for more details.
 */

#ifndef	_UDMU_PRIV_H
#define	_UDMU_PRIV_H

/*
 * Declarations and definitions copied from the ZFS source. This has
 * been done to avoid including the private headers to compile PIOS.
 */

extern const char *spa_config_dir;
#define	FTAG ((char *)__func__)
#define	TXG_WAIT		1ULL
#define TXG_NOWAIT		2ULL
#define	DS_MODE_STANDARD	1	/* normal access, no special needs */
#define	DMU_NEW_OBJECT	(-1ULL)
#define	SPA_MINBLOCKSHIFT	9


typedef enum dmu_objset_type {
	DMU_OST_NONE,
	DMU_OST_META,
	DMU_OST_ZFS,
	DMU_OST_ZVOL,
	DMU_OST_OTHER,			/* For testing only! */
	DMU_OST_ANY,			/* Be careful! */
	DMU_OST_NUMTYPES
} dmu_objset_type_t;

typedef enum dmu_object_type {
	DMU_OT_NONE,
	/* general: */
	DMU_OT_OBJECT_DIRECTORY,	/* ZAP */
	DMU_OT_OBJECT_ARRAY,		/* UINT64 */
	DMU_OT_PACKED_NVLIST,		/* UINT8 (XDR by nvlist_pack/unpack) */
	DMU_OT_PACKED_NVLIST_SIZE,	/* UINT64 */
	DMU_OT_BPLIST,			/* UINT64 */
	DMU_OT_BPLIST_HDR,		/* UINT64 */
	/* spa: */
	DMU_OT_SPACE_MAP_HEADER,	/* UINT64 */
	DMU_OT_SPACE_MAP,		/* UINT64 */
	/* zil: */
	DMU_OT_INTENT_LOG,		/* UINT64 */
	/* dmu: */
	DMU_OT_DNODE,			/* DNODE */
	DMU_OT_OBJSET,			/* OBJSET */
	/* dsl: */
	DMU_OT_DSL_DIR,			/* UINT64 */
	DMU_OT_DSL_DIR_CHILD_MAP,	/* ZAP */
	DMU_OT_DSL_DS_SNAP_MAP,		/* ZAP */
	DMU_OT_DSL_PROPS,		/* ZAP */
	DMU_OT_DSL_DATASET,		/* UINT64 */
	/* zpl: */
	DMU_OT_ZNODE,			/* ZNODE */
	DMU_OT_ACL,			/* ACL */
	DMU_OT_PLAIN_FILE_CONTENTS,	/* UINT8 */
	DMU_OT_DIRECTORY_CONTENTS,	/* ZAP */
	DMU_OT_MASTER_NODE,		/* ZAP */
	DMU_OT_DELETE_QUEUE,		/* ZAP */
	/* zvol: */
	DMU_OT_ZVOL,			/* UINT8 */
	DMU_OT_ZVOL_PROP,		/* ZAP */
	/* other; for testing only! */
	DMU_OT_PLAIN_OTHER,		/* UINT8 */
	DMU_OT_UINT64_OTHER,		/* UINT64 */
	DMU_OT_ZAP_OTHER,		/* ZAP */
	/* new object types: */
	DMU_OT_ERROR_LOG,		/* ZAP */

	DMU_OT_NUMTYPES
} dmu_object_type_t;

struct objset;
struct dmu_tx;
struct dmu_object_info;
struct dsl_pool;
struct nvlist;

extern void kernel_init(int );

extern int spa_export(char *pool, struct nvlist **oldconfig);

extern int spa_import(const char *pool, struct nvlist *config, const char *altroot);

extern int dmu_objset_open(const char *name, dmu_objset_type_t type, int mode,
    struct objset **osp);

extern void dmu_objset_close(struct objset *os);

extern struct dmu_tx *dmu_tx_create(struct objset *os);

extern void dmu_tx_hold_write(struct dmu_tx *tx, uint64_t object,
	uint64_t off, int len);

extern void dmu_tx_abort(struct dmu_tx *tx);

extern int dmu_tx_assign(struct dmu_tx *tx, uint64_t txg_how);

extern void dmu_tx_wait(struct dmu_tx *tx);

extern void dmu_tx_commit(struct dmu_tx *tx);

extern uint64_t dmu_object_alloc(struct objset *os, dmu_object_type_t ot,
	int blocksize, dmu_object_type_t bonus_type, int bonus_len,
	struct dmu_tx *tx);

extern int dmu_object_info(struct objset *os, uint64_t object,
	struct dmu_object_info *doi);

extern int dmu_read(struct objset *os, uint64_t object, uint64_t offset,
	uint64_t size, void *buf);

extern void dmu_write(struct objset *os, uint64_t object, uint64_t offset,
	uint64_t size, const void *buf, struct dmu_tx *tx);

extern void txg_wait_synced(struct dsl_pool *dp, uint64_t txg);

extern struct dsl_pool *dmu_objset_pool(struct objset *os);

extern int dmu_object_set_blocksize(struct objset *os, uint64_t object,
	uint64_t size, int ibs, struct dmu_tx *tx);

#endif /* _UDMU_PRIV_H */
