/* -*- mode: c; c-basic-offset: 8; indent-tabs-mode: nil; -*-
 * vim:expandtab:shiftwidth=8:tabstop=8:
 *
 *  udmu/udmu.c
 *  Module that interacts with the ZFS DMU.
 *
 *  Copyright (c) 2006 Cluster File Systems, Inc.
 *   Author: Manoj Joseph <manoj@clusterfs.com>
 *
 *   This file is part of the Lustre file system, http://www.lustre.org
 *   Lustre is a trademark of Cluster File Systems, Inc.
 *
 *   You may have signed or agreed to another license before downloading
 *   this software.  If so, you are bound by the terms and conditions
 *   of that agreement, and the following does not apply to you.  See the
 *   LICENSE file included with this distribution for more information.
 *
 *   If you did not agree to a different license, then this copy of Lustre
 *   is open source software; you can redistribute it and/or modify it
 *   under the terms of version 2 of the GNU General Public License as
 *   published by the Free Software Foundation.
 *
 *   In either case, Lustre is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   license text for more details.
 */

#include <stdio.h>
#include <stdio_ext.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/param.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/file.h>
#include <stdarg.h>
#include <stdint.h>
#include <assert.h>

#if defined(linux)
#define FREAD           0x01
#define FWRITE          0x02
#endif

#include "udmu.h"
#include "udmu_priv.h"


#define OBJ_SIZE 64
#define	FATAL_MSG_SZ	1024
#define CONFIG_DIR "/etc/zfs/udmu"
char configdir[MAXPATHLEN];
int debug_level = 0;
int how = TXG_WAIT;

#ifdef ENABLE_DMU 

static void
dbg_printf(FILE *stream, char *message, ...)
{
	va_list args;

	if (debug_level != 0) {
		va_start(args, message);
		(void) vfprintf(stream, message, args);
		va_end(args);
	}
}

void
udmu_init()
{
	char cmd[MAXPATHLEN];
	struct rlimit rl = { 1024, 1024 };

	/*
	 * Copy zpool.cache to /etc/zfs/udmu/$pid and set the
	 * spa_config_dir to this tmp folder. udmu should not modify
	 * the global zpool.cache file.
	 */
	snprintf(configdir, MAXPATHLEN, "%s/%d", CONFIG_DIR, (int)getpid());

	snprintf(cmd, MAXPATHLEN, "mkdir -p %s", configdir);
	system(cmd);

	snprintf(cmd, MAXPATHLEN, "cp /etc/zfs/zpool.cache %s/zpool.cache",
	    configdir); 
	system(cmd);

	spa_config_dir = configdir;

	(void) setvbuf(stdout, NULL, _IOLBF, 0);
	(void) setrlimit(RLIMIT_NOFILE, &rl);
	// (void) enable_extended_FILE_stdio(-1, -1);
	debug_level = 0;

	/* Initialize the emulation of kernel services in userland. */
	kernel_init(FREAD | FWRITE);
}

void
udmu_debug(int level)
{
	debug_level = level;	
}

int
udmu_open_objset(char *osname, udmu_objset_t *uos)
{
	int error;
	char cmd[MAXPATHLEN];
	struct nvlist *config;

	/*
	 * Export the pool so that the kernel DMU and the userland DMU
	 * do not access it simultaneously.
	 */
	if (debug_level) {
		dbg_printf(stdout, "Exprting pool %s\n", osname);
	}
	snprintf(cmd, MAXPATHLEN, "zpool export %s", osname);
	error = system(cmd);

	if (error) {
		dbg_printf(stderr, "zpool export %s failed: %d\n",
		    osname, error);
		return(error);

	}

	error = spa_export(osname, &config);
	if (error) {
		dbg_printf(stderr, "spa_export('%s') failed: %d\n",
		    osname, error);
		return(error);
	}

	error = spa_import(osname, config, NULL);
	if (error) {
		dbg_printf(stderr, "spa_import('%s') failed: %d\n", osname, error);
		return(error);
	}

	/*
	 * Open the imported pool
	 */
	error = dmu_objset_open(osname, DMU_OST_ZFS, DS_MODE_STANDARD, &uos->os);
	if (error) {
		dbg_printf(stderr, "dmu_objset_open() failed: %d", error);
		return (error);
	}
	uos->zilog = 0;

	return (0);
}

void
udmu_close_objset(udmu_objset_t uos)
{
	dmu_objset_close(uos.os);
	uos.os = NULL;
}

uint64_t
udmu_object_create(udmu_objset_t uos)
{
	struct dmu_tx *tx;
	int error;
	uint64_t object = 0ULL;

	tx = dmu_tx_create(uos.os);
	dmu_tx_hold_write(tx, DMU_NEW_OBJECT, 0, OBJ_SIZE);
	error = dmu_tx_assign(tx, TXG_WAIT);
	if (error) {
		dmu_tx_abort(tx);
		return (object);
	}

	object = dmu_object_alloc(uos.os, DMU_OT_UINT64_OTHER, 0,
		    DMU_OT_NONE, 0, tx);
	error = dmu_object_set_blocksize(uos.os, object,
	    128ULL << 10, 0, tx);
	if (error) {
		dmu_tx_abort(tx);
		return (object);
	}

	dmu_tx_commit(tx);

	return (object);
}

int
udmu_object_open(udmu_objset_t uos, uint64_t object)
{
	int error;
	error = dmu_object_info(uos.os, object, NULL);
	return (error);
}

int
udmu_write(udmu_objset_t uos, uint64_t object, uint64_t offset, uint64_t size,
    const void *buf)
{
	struct dmu_tx *tx;
	int error;

	dbg_printf(stdout, "udmu_write(%lld, %lld, %lld\n",
		   object, offset, size);

	while (1) {
		tx = dmu_tx_create(uos.os);
		dmu_tx_hold_write(tx, object, offset, size);
		error = dmu_tx_assign(tx, how);

		if (error) {
			if (error == ERESTART && how == TXG_NOWAIT) {
				dmu_tx_wait(tx);
				dmu_tx_abort(tx);
				continue;
			}
			dbg_printf(stderr, "Error in dmu_tx_assign(). %d",
			           error);
			dmu_tx_abort(tx);
			return (error);
		}
		break;
	}

	dmu_write(uos.os, object, offset, size, buf, tx);
	dmu_tx_commit(tx);

	return 0;
}

int
udmu_read(udmu_objset_t uos, uint64_t object, uint64_t offset, uint64_t size,
    void *buf)
{
	int error;

	error = dmu_read(uos.os, object, offset, size, buf);

	return (error);
}

void
udmu_sync(udmu_objset_t uos)
{
	if (uos.zilog) {
		// zil_commit(uos.zilog, UINT64_MAX, FSYNC);
	}
	txg_wait_synced(dmu_objset_pool(uos.os), 0ULL);
}

#else /* !ENABLE_DMU */

/* Stubs that are used while compiling with out ZFS DMU support */

void udmu_init()
{
	assert(0);
}

void udmu_debug(int level)
{
	assert(0);
}

int udmu_open_objset(char *osname, udmu_objset_t *uos)
{
	assert(0);
}

void udmu_close_objset(udmu_objset_t uos)
{
	assert(0);
}

uint64_t udmu_object_create(udmu_objset_t uos)
{
	assert(0);
}

int udmu_object_open(udmu_objset_t uos, uint64_t object)
{
	assert(0);
}

int udmu_read(udmu_objset_t uos, uint64_t object, uint64_t offset,
		     uint64_t size, void *buf)
{
	assert(0);
}

int udmu_write(udmu_objset_t uos, uint64_t object, uint64_t offset,
	       uint64_t size, const void *buf)
{
	assert(0);
}

void udmu_sync(udmu_objset_t uos)
{
	assert(0);
}

#endif /* ENABLE_DMU */
